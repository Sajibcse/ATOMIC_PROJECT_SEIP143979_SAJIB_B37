<?php
namespace App\BITM\SEIP143979\BookTitle;

use App\BITM\SEIP143979\Message\Message;
use App\BITM\SEIP143979\Utility\Utility;
use App\BITM\SEIP143979\Model\Database as DB;
use PDO;

class BookTitle extends DB
{
    public $id="";
    public $book_title="";
    public $author_name="";
    public $checkbox;



    public function __construct(){
        parent::__construct();
    }

//method 01//
    /**
     * @param string $fetchMode
     * @return array
     */
    public function index($fetchMode='ASSOC'){
        $fetchMode = strtoupper($fetchMode);
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from book_title WHERE is_delete='0'");
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);


        $all_books=$sth->fetchAll();

        return  $all_books;
    }//end of index method


//method 02
    public function view($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from book_title Where id=$id");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $selected_book=$sth->fetch();

        return  $selected_book;

    }//end of view method


//method 03//
    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('book_title',$data)){
            $this->book_title=$data['book_title'];

        }
        if(array_key_exists('author_name',$data)){
            $this->author_name=$data['author_name'];

        }
        if(array_key_exists('checkbox',$data))
        {
            $this->checkbox = $data['checkbox'];
        }
    }//end of set data method


    //method4
    public function store(){
        $dbh=$this->connection;
        $values=array($this->book_title,$this->author_name);
        $query="insert into book_title(book_title,author_name) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ BookTitle: $this->book_title ] ,
               [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('create.php');



    }//EOF store

    //method5
    public function update(){
        $dbh=$this->connection;
        $values=array($this->book_title,$this->author_name);

      /* var_dump($values);die;*/


        $query='UPDATE `book_title`  SET `book_title`  = ? , `author_name` = ? where `book_title`.`id` = '.$this->id;

        $sth=$dbh->prepare($query);
       /* var_dump($query);die();*/
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ BookTitle: $this->book_title ] ,
               [ AuthorName: $this->author_name ] <br> Data Has Been Updated Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');



    }//EOF update


    //method6
    public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from book_title Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }//EOF delete



    //method 7
    public function trash($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE book_title  SET is_delete  = "1" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been trashed Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been trashed !</h3></div>");
        Utility::redirect('index.php');



    }//end of trash method


    //method 8
    public function trashed($fetchMode='ASSOC'){
        $fetchMode = strtoupper($fetchMode);
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from book_title WHERE is_delete='1'");
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);


        $all_books=$sth->fetchAll();

        return  $all_books;
    }//end of trashed method which show the trashed list



    public function count($fetchMode='ASSOC'){
        $DBH=$this->connection;
        $query="SELECT COUNT(*) AS totalItem FROM `atomic_project_b37`.`book_title` WHERE `is_delete`=0 ";
        $sth=$DBH->prepare($query);
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);


        $row=$sth->fetchAll();


       /* $result=mysqli_query($this->conn,$query);*/
       /* $row= mysqli_fetch_assoc($sth);*/
        return $row['totalItem'];
    }//end of count

    public function mulSoftDelete()

    {
        $checkbox = $_POST['checkbox'];
        for ($i = 0; $i < count($checkbox);$i++) {
            $query = $this->connection->prepare("UPDATE book_title SET is_delete='1' WHERE id='$checkbox[$i]'");
            var_dump($query);
            $query->execute();
            if ($query) {
                Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ BookTitle: $this->book_title] , [ AuthorName: $this->author_name ] <br> Data Has Been Deleted Successfully!</h3></div>");

            } else {
                Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Not Been Deleted Successfully!</h3></div>");

            }
            Utility::redirect("multipleDelete.php");
        }
    }//end of multiple trash delete

    public function deleteMultiple()
    {
        $checkbox = $_POST['checkbox'];
        for ($i = 0; $i < count($checkbox);$i++)
        {

            $query =$this->connection->prepare("delete  from book_title WHERE id='$checkbox[$i]'");
            var_dump($query);
            $query->execute();

            if ($query)
            {
                Message::message("<div class=\"alert alert-info\"><strong>Deleted!</strong> Selected Data has been deleted successfully.</div>");
                Utility::redirect("index.php");
            } 
            else
                {
                Message::message("<div class=\"alert alert-info\"> <strong>Deleted!</strong> Selected Data has not been deleted successfully.</div>");
                Utility::redirect("index.php");
            }

        }
    }



    public function recover($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE book_title  SET is_delete  = "0" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been recovered Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been recovered !</h3></div>");
        Utility::redirect('recovery.php');



    }///EOF recover

    public function mulRecover()

    {
        $checkbox = $_POST['checkbox'];
       /* var_dump($checkbox);die();*/
        for ($i = 0; $i < count($checkbox);$i++) {
            $query = $this->connection->prepare("UPDATE book_title SET is_delete='0' WHERE id='$checkbox[$i]'");
            var_dump($query);
            $query->execute();
            if ($query) {
                Message::message("<div class='alert alert-success' id='msg'><h3 align='center'> Data Has Been Recoverd Successfully!</h3></div>");

            } else {
                Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Not Been Deleted Successfully!</h3></div>");

            }
            Utility::redirect("recovery.php");
        }
    }//EOF mulRecover


    public function indexPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE is_delete = '0' LIMIT $start,$itemsPerPage";

        $STH = $this->connection->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();


    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_delete` ='0' AND (`book_title` LIKE '%".$requestArray['search']."%' OR `author_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `book_title` WHERE `is_delete` ='0' AND `book_title` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_delete` ='0' AND `author_name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $all_books = $STH->fetchAll();

        return $all_books;

    }// end of search()


    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `book_title` WHERE `is_delete` ='0'";

        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $all_books= $STH->fetchAll();
        foreach ($all_books as $oneData) {
            $_allKeywords[] = trim($oneData->book_title);
        }

        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $all_books= $STH->fetchAll();
        foreach ($all_books as $oneData) {

            $eachString= strip_tags($oneData->book_title);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $all_books= $STH->fetchAll();
        foreach ($all_books as $oneData) {
            $_allKeywords[] = trim($oneData->author_name);
        }
        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $all_books= $STH->fetchAll();
        foreach ($all_books as $oneData) {

            $eachString= strip_tags($oneData->author_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords





}//end of booktitle class

