<?php
namespace App\BITM\SEIP143979\SummaryOfOrganization;
use App\BITM\SEIP143979\Message\Message;
use App\BITM\SEIP143979\Utility\Utility;
use App\BITM\SEIP143979\Model\Database as DB;
use PDO;

class SummaryOfOrganization extends DB
{
    public $id="";
    public $organization_name="";
    public $organization_summary="";



    public function __construct()
    {
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }


    public function setData($data=null)
    {

        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('organization_name',$data)){
            $this->organization_name=$data['organization_name'];

        }
        if(array_key_exists('organization_summary',$data)){
            $this->organization_summary=$data['organization_summary'];

        }
        if(array_key_exists('checkbox',$data))
        {
            $this->checkbox = $data['checkbox'];
        }
    }

    public function store(){
        $dbh=$this->connection;
        $values=array($this->organization_name,$this->organization_summary);
        $query="insert into summary_of_organization(organization_name,organization_summary) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
            Message::message("<div id='msg'><h3 align='center'>[ Organization name: $this->organization_name ] ,
 [ organization_summary: $this->organization_summary ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else         Message::message("<div id='msg'><h3 align='center'>[ Organization name: $this->organization_name ] ,
 [ organization_summary: $this->organization_summary ] <br> Data Hasn't Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');



    }



    public function index($fetchMode='ASSOC')
    {
            $fetchMode = strtoupper($fetchMode);
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from summary_of_organization where is_delete='0'");
            $sth->execute();
            if(substr_count($fetchMode,'OBJ') > 0)
                $sth->setFetchMode(PDO::FETCH_OBJ);
            else
                $sth->setFetchMode(PDO::FETCH_ASSOC);

            $all_organization=$sth->fetchAll();


            return  $all_organization;
        }







    public function view($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from summary_of_organization Where id=$id");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $selected_organization=$sth->fetch();

        return  $selected_organization;

    }




    public function update(){
        $dbh=$this->connection;
     $values=array($this->organization_name,$this->organization_summary);

        //var_dump($values);


        /*$query='UPDATE summary_of_organization  SET organization_name  = ?   , organization_summary = ? where id ='.$this->id;*/
        $query='UPDATE summary_of_organization SET organization_name= ? , organization_summary= ?  WHERE id = '.$this->id ;
        $sth=$dbh->prepare($query);
        $sth->execute($values);
       /* var_dump($values);die;*/
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ Organization name: $this->organization_name ] ,
               [ Organization Summary: $this->organization_summary ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");

        Utility::redirect('index.php');



    }
    public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from summary_of_organization Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }


    public function trash($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE summary_of_organization  SET is_delete  = "1" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been trashed Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been trashed !</h3></div>");
        Utility::redirect('index.php');



    }


    //method 8
    public function trashed($fetchMode='ASSOC'){
        $fetchMode = strtoupper($fetchMode);
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from summary_of_organization WHERE is_delete='1'");
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);


        $allData=$sth->fetchAll();

        return  $allData;
    }//end of trashed method which show the trashed list


    //METHOD 9
    public function count($fetchMode='ASSOC'){
        $DBH=$this->connection;
        $query="SELECT COUNT(*) AS totalItem FROM `atomic_project_b37`.`summary_of_organization` WHERE `is_delete`=0 ";
        $sth=$DBH->prepare($query);
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);


        $row=$sth->fetchAll();


        /* $result=mysqli_query($this->conn,$query);*/
        /* $row= mysqli_fetch_assoc($sth);*/
        return $row['totalItem'];
    }//end of count

    public function mulSoftDelete()

    {
        $checkbox = $_POST['checkbox'];
        for ($i = 0; $i < count($checkbox);$i++) {
            $query = $this->connection->prepare("UPDATE summary_of_organization SET is_delete='1' WHERE id='$checkbox[$i]'");
            var_dump($query);
            $query->execute();
            if ($query) {
                Message::message("<div class='alert alert-success' id='msg'><h3 align='center'> Data Has Been Deleted Successfully!</h3></div>");

            } else {
                Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'> Data Has Not Been Deleted Successfully!</h3></div>");

            }
            Utility::redirect("multipleDelete.php");
        }
    }//end of multiple trash delete

    public function deleteMultiple($id)
    {
        $checkbox = $_POST['checkbox'];
        for ($i = 0; $i < count($checkbox);$i++)
        {

            $query =$this->connection->prepare("delete  from summary_of_organization WHERE id='$checkbox[$i]'");
            var_dump($query);
            $query->execute();

            if ($query)
            {
                Message::message("<div class=\"alert alert-info\"><strong>Deleted!</strong> Selected Data has been deleted successfully.</div>");
                Utility::redirect("index.php");
            }
            else
            {
                Message::message("<div class=\"alert alert-info\"> <strong>Deleted!</strong> Selected Data has not been deleted successfully.</div>");
                Utility::redirect("index.php");
            }

        }
    }



    public function recover($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE summaray_of_organization SET is_delete  = "0" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been recovered Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been recovered !</h3></div>");
        Utility::redirect('recovery.php');



    }///EOF recover

    public function mulRecover()

    {
        $checkbox = $_POST['checkbox'];
        /* var_dump($checkbox);die();*/
        for ($i = 0; $i < count($checkbox);$i++) {
            $query = $this->connection->prepare("UPDATE summary_of_organization SET is_delete='0' WHERE id='$checkbox[$i]'");
            var_dump($query);
            $query->execute();
            if ($query) {
                Message::message("<div class='alert alert-success' id='msg'><h3 align='center'> Data Has Been Recoverd Successfully!</h3></div>");

            } else {
                Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Not Been Deleted Successfully!</h3></div>");

            }
            Utility::redirect("recovery.php");
        }
    }//EOF mulRecover


    public function indexPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from summary_of_organization  WHERE is_delete = '0' LIMIT $start,$itemsPerPage";

        $STH = $this->connection->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        /*var_dump($arrSomeData);
        die();*/
        return $arrSomeData;

    }// end of indexPaginator()



    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) )  $sql = "SELECT * FROM `summary_of_organization` WHERE `is_delete` ='0' AND (`organization_name` LIKE '%".$requestArray['search'].$requestArray['search']."%')";
        if(isset($requestArray['byName'])  ) $sql = "SELECT * FROM `summary_of_organization` WHERE `is_delete` ='0' AND `organization_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) )  $sql = "SELECT * FROM `summary_of_organization` WHERE `is_delete` ='0' AND `organization_name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `summary_of_organization` WHERE `is_delete` ='0'";

        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->organization_name);
        }

        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->organization_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        /*// for each search field block start
        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->author_name);
        }
        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $all_books= $STH->fetchAll();
        foreach ($all_books as $oneData) {

            $eachString= strip_tags($oneData->author_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end*/


        return array_unique($_allKeywords);


    }// get all keywords







}

