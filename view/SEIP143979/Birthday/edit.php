<?php
require_once("../../../vendor/autoload.php");

use App\BITM\SEIP143979\Birthday\Birthday;


$obj= new Birthday();
$id=$_GET['id'];



$selected_person= $obj->view($id);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/birthday.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
</head>
<body>


<div class="container">


    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Update Birthday</h1>

                        <div class="row-fluid user-row">
                            <img src="../../../resource/assets/images/birthday-109a.jpg" class="img-responsive icon" alt="Conxole Admin"/>
                        </div>
                    </div>




                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-birthday" method="Post" action="update.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <input class="form-control" value="<?php echo $selected_person['name'] ?>"  name="name" type="text">
                            <input class="form-control" value="<?php echo $selected_person['birthday'] ?>" name="birthday" type="date" id="datepicker">
                            </br>
                            <input type="hidden" name="id" value="<?php echo $id?>" >
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Update »">
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>





</body>
</html>